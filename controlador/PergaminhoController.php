<?php
    include_once 'modelo/Pergaminho.php';
    
    class PergaminhoController {
  
        private $pergaminho;

        public function __construct($pergaminho) {
            $this->pergaminho = $pergaminho;
        }

        /**
         * Baseado no textoB, procura o padrão de uma preposição em googlon alimentando um contador
         */
        public function extrairPreposicoes() {
            $textoB = $this->pergaminho->getPergaminho();
            $palavras = explode(" ", $textoB);
            $totalPreposicoes = 0;

            foreach($palavras as $key => $value) {
                if((strlen($value) == 4) && (in_array(substr($value, -1), $this->pergaminho->getTipoFoo())) && !(mb_strpos($value, 'l') !== false)) 
                    $totalPreposicoes++;
            }
            
            return $totalPreposicoes;
        }

        /**
         * Baseado no textoB, procura o padrão de um verbo em googlon alimentando um contador
         */
        public function extrairVerbos(){
            $textoB = $this->pergaminho->getPergaminho();
            $palavras = explode(" ", $textoB);
            $totalVerbos = 0;
            $totalVerbosPP = 0;

            foreach($palavras as $key => $value) {
                if((strlen($value) >= 7) && (!in_array(substr($value, -1), $this->pergaminho->getTipoFoo()))) {
                    $totalVerbos++;
                    if(in_array($value[0], $this->pergaminho->getTipoFoo()))
                        $totalVerbosPP++;
                }
            }

            return array("verbos" => $totalVerbos, "verbos_primeira_pessoa" => $totalVerbosPP);
        }

        /**
         * Baseado no textoB, gera um vocabulario de acordo com as regras de alfabeto e ordanação em googlon
         */
        public function gerarVocabulário() {
            $textoB = $this->pergaminho->getPergaminho();
            $palavras = explode(" ", $textoB);
            $colecao = array();
            foreach($palavras as $texto){
                $colecao[] = $this->googlonToPortugues($texto);
            }
            sort($colecao);
            foreach($colecao as $item){
                $retorno[] = $this->portuguesToGooglon($item);
            }
            
            return $retorno;
            
        }

        /**
         * Recebe parametro com uma palavra do texto e ordena de acordo com o alfabeto em portugues
         */
        public function googlonToPortugues($palavra)
        {
            $palavraPortugues = array();
            for($i = 0; $i < strlen($palavra); $i ++){
                foreach($this->pergaminho->getAlfabetoGooglon() as $letraGooglon =>  $letraPortugues){
                    if($letraGooglon == substr($palavra,$i,1)){
                        $palavraEmPortugues[] = (string) "$letraPortugues";
                    }
                } 
            }
            return  $palavraEmPortugues = implode("", $palavraEmPortugues);
        }
    
        /**
         * * Recebe parametro com uma palavra e ordena de acordo com o alfabeto em googlon
         */
        public function portuguesToGooglon($palavra)
        {
            $palavraEmGooglon = array();
            for($i = 0; $i < strlen($palavra); $i ++){
                
                foreach($this->pergaminho->getAlfabetoGooglon() as $letraGooglon => $letraPortugues){
                    if($letraPortugues == substr($palavra,$i,1)){
                        $palavraEmGooglon[] = (string) "$letraGooglon";
                    }
                } 
            }
            return  $palavraEmGooglon = implode("", $palavraEmGooglon);
        
        }

    }
?>