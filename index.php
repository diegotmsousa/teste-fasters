<?php
    include_once 'modelo/Pergaminho.php';
    include_once 'controlador/PergaminhoController.php';

    
    $quebra = "<br>";
    $linha = "<hr>";
    $obj = new PergaminhoController(new Pergaminho());

    echo $quebra;
    echo "*** TESTE FASTERS *** " . $quebra;
    echo "Desenvolvedor: Diego Sousa" . $quebra;
    echo $quebra . $linha;

    echo "Questão 1" . $quebra;
    echo "No Texto B, quantas preposições existem?<br>";
    echo "R: " . $obj->extrairPreposicoes() . $quebra;

    echo $quebra . $linha;

    $verbos = $obj->extrairVerbos();
    echo "Questão 2" . $quebra;
    echo "No Texto B, quantos são os verbos?" . $quebra;
    echo "R: " . $verbos["verbos"] . $quebra;

    echo $quebra ;
    echo "E quantos verbos do Texto B estão em primeira pessoa?" . $quebra;
    echo "R: " .  $verbos["verbos_primeira_pessoa"];


    echo $quebra . $linha;

    echo "Questão 3" . $quebra;
    echo "Como seria a lista de vocabulário do Texto B?" . $quebra;
    echo "R: " . $quebra . implode(" ", $obj->gerarVocabulário()) . $quebra;


?>